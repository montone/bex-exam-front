import React from 'react';
import PropTypes from 'prop-types';

import { Button } from './styles';

function ButtonBasic({ children, color, type, stretch, ...rest }) {
  return (
    <Button type={type} color={color} stretch={stretch} {...rest}>
      {children}
    </Button>
  );
}

ButtonBasic.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.node,
  ]).isRequired,
  color: PropTypes.string,
  type: PropTypes.string,
  stretch: PropTypes.bool,
};

ButtonBasic.defaultProps = {
  color: 'primary',
  type: 'button',
  stretch: false,
};

export default ButtonBasic;
