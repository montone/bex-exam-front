import styled from 'styled-components';

import { darken } from 'polished';

import colors from '~/styles/colors';

export const Button = styled.button`
  width: ${(props) => (props.stretch ? '100%' : 'auto')};
  min-width: 246px;
  height: 51px;
  padding: 0 16px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.0625em;
  font-weight: 500;
  text-transform: uppercase;
  color: ${colors.white};
  border-radius: 10px;
  background-color: ${(props) => {
    switch (props.color) {
      case 'primary':
        return colors.primary;
      case 'secondary':
        return colors.secondary;
      case 'info':
        return colors.statusInfo;
      case 'success':
        return colors.statusSuccess;
      case 'danger':
        return colors.statusDanger;
      default:
        return colors.primary;
    }
  }};

  :hover {
    background: ${darken(0.1, colors.primary)};
  }

  :active {
    transform: translate(1px, 1px);
  }

  cursor: pointer;
`;
