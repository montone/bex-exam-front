import styled from 'styled-components';

import colors from '~/styles/colors';

export const Wrapper = styled.div`
  width: 100%;
  height: 100%;
  min-height: 100vh;
  min-width: 100vw;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: ${colors.greyLight};
`;
