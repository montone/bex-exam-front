import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useField } from '@unform/core';

import { MdInfo } from 'react-icons/md';

import { Label, LabelText, StyledInput, MoreInfo, Error } from './styles';

export default function Input({ name, label, info, ...rest }) {
  const [pushLabel, setPushLabel] = useState(false);
  const inputRef = useRef(null);
  const { fieldName, registerField, defaultValue, error } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
    });
  }, [fieldName, registerField]);

  useEffect(() => {
    inputRef.current.addEventListener('focus', () => {
      if (!pushLabel) setPushLabel(true);
    });
  }, [pushLabel]);

  function handleClick() {
    setPushLabel(true);
    inputRef.current.focus();
    inputRef.current.addEventListener('blur', () => {
      if (inputRef.current.value === '') {
        setPushLabel(false);
      }
    });
  }

  return (
    <Label htmlFor={name}>
      <LabelText className={pushLabel ? 'push' : ''} onClick={handleClick}>
        {label}
        {info && (
          <MoreInfo>
            <MdInfo size="13" />
          </MoreInfo>
        )}
      </LabelText>

      <StyledInput
        name={name}
        ref={inputRef}
        defaultValue={defaultValue}
        {...rest}
      />

      {error && <Error>{error}</Error>}
    </Label>
  );
}

Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  info: PropTypes.string,
};

Input.defaultProps = {
  label: 'Número do Cartão',
  info: '',
};
