import styled from 'styled-components';

import colors from '~/styles/colors';

export const Label = styled.label`
  position: relative;
  display: block;
  width: 100%;
  height: 36px;
  background-color: rgba(255, 255, 255, 0);
`;

export const LabelText = styled.div`
  position: absolute;
  width: 100%;
  height: 30px;
  top: 6px;
  display: flex;
  align-items: center;
  font-size: 1.0625em;
  color: ${colors.greyMedium};
  transition: all 0.3s ease;

  &.push {
    font-size: 0.8125em;
    top: -10px;
    height: 13px;
  }
`;

export const MoreInfo = styled.span`
  width: 13px;
  height: 13px;
  margin-left: 2px;
  transform: translateY(2px);
`;

export const StyledInput = styled.input`
  width: 100%;
  height: 36px;
  font-size: 1.0625em;
  border-bottom: 1px solid ${colors.greyMedium};
  background-color: rgba(255, 255, 255, 0);
`;

export const Error = styled.span`
  position: absolute;
  width: 100%;
  left: 0;
  top: 35px;
  padding-top: 2px;
  border-top: 1px solid ${colors.statusDanger};
  font-size: 0.6875em;
  color: ${colors.statusDanger};
`;
