import styled from 'styled-components';

import colors from '~/styles/colors';

export const StyledSelect = styled.div`
  position: relative;
  width: 100%;
  height: 36px;
  font-size: 1.0625em;
  border-bottom: 1px solid ${colors.greyMedium};
  margin-bottom: 36px;

  select {
    position: relative;
    width: 100%;
    height: 36px;
    background-color: none;
    border: none;
    appearance: none;
    background-color: rgba(0, 0, 0, 0);
    z-index: 1;
  }
`;

export const SelectArrow = styled.div`
  position: absolute;
  height: 36px;
  width: 36px;
  top: 0;
  right: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  margin-left: 20px;
  z-index: 0;
`;

export const LabelText = styled.div`
  position: absolute;
  top: 6px;
  font-size: 1.0625em;
  color: ${colors.greyMedium};
  transition: all 0.3s ease;

  &.push {
    font-size: 0.8125em;
    top: -10px;
  }
`;

export const Error = styled.span`
  position: absolute;
  width: 100%;
  left: 0;
  top: 35px;
  padding-top: 2px;
  border-top: 1px solid ${colors.statusDanger};
  font-size: 0.6875em;
  color: ${colors.statusDanger};
`;
