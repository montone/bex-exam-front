import React, { useState, useRef, useEffect } from 'react';
import { useField } from '@unform/core';
import PropTypes from 'prop-types';

import { MdExpandMore } from 'react-icons/md';

import { StyledSelect, LabelText, SelectArrow, Error } from './styles';
import colors from '~/styles/colors';

function Select({ name, options, firstEmpty, ...rest }) {
  const [empty, setEmpty] = useState(true);
  const selectRef = useRef(null);
  const { fieldName, registerField, defaultValue, error } = useField(name);

  useEffect(() => {
    registerField({
      name: fieldName,
      ref: selectRef.current,
      path: 'state.value',
      getValue: (ref) => {
        return ref.value;
      },
    });
  }, [fieldName, registerField, rest.isMulti]);

  function handleUpdate(e) {
    if (e.target.value !== '') setEmpty(false);
  }

  return (
    <StyledSelect>
      <LabelText className={!empty ? 'push' : ''}>Número de Parcelas</LabelText>

      <select
        name={name}
        ref={selectRef}
        defaultValue={defaultValue}
        {...rest}
        onChange={(e) => handleUpdate(e)}
      >
        {firstEmpty && <option defaultValue>&nbsp;</option>}
        {options.map((item, index) => {
          return <option key={String(index)}>{item}</option>;
        })}
      </select>

      <SelectArrow>
        <MdExpandMore color={colors.primary} size="24" />
      </SelectArrow>

      {error && <Error>{error}</Error>}
    </StyledSelect>
  );
}

Select.propTypes = {
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.oneOfType([PropTypes.number, PropTypes.string])
  ),
  firstEmpty: PropTypes.bool,
};

Select.defaultProps = {
  options: [''],
  firstEmpty: true,
};

export default Select;
