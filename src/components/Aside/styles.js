import styled from 'styled-components';

import colors from '~/styles/colors';
import { device } from '~/styles/queries';

export const Container = styled.div`
  display: none;
  background: ${colors.white};

  @media ${device.desktopL} {
    display: flex;
    width: 327px;
    height: 285px;
  }
`;
