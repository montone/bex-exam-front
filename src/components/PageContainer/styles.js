import styled from 'styled-components';

import { device } from '~/styles/queries';

export const Container = styled.div`
  width: 100%;
  height: 100%;
  flex: 1;
  display: flex;

  @media ${device.laptop} {
    max-width: 1367px;
  }
`;
