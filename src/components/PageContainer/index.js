import React from 'react';
import PropTypes from 'prop-types';

import { Container } from './styles';

function PageContainer({ children, ...rest }) {
  return <Container {...rest}>{children}</Container>;
}

PageContainer.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.node,
  ]).isRequired,
};

export default PageContainer;
