import styled from 'styled-components';

import ButtonBasic from '~/components/ButtonBasic';

import colors from '~/styles/colors';
import { device } from '~/styles/queries';

export const Section = styled.section`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  min-height: 100%;
  display: flex;
  flex-direction: column;
  background-color: ${colors.white};

  @media ${device.laptop} {
    position: relative;
    width: 1024px;
    height: 596px;
    top: unset;
    left: unset;
    flex-direction: row;
  }
`;

export const CardContainer = styled.div`
  position: relative;
  width: 100%;
  height: 239px;
  padding: 40px 40px 0 40px;
  color: ${colors.white};
  display: flex;
  flex-direction: column;
  text-align: center;
  background-color: ${colors.primary};

  @media ${device.laptop} {
    padding: 64px 16px 40px 68px;
    width: 352px;
    height: 100%;
  }
`;

export const BreadCrumb = styled.div`
  position: relative;
  margin-bottom: 30px;
  font-size: 0.8125em;
  line-height: 20px;
  display: flex;
  flex-direction: row;
  justify-content: center;

  @media ${device.laptop} {
    justify-content: flex-start;
  }

  .steps {
    display: block;

    @media ${device.laptop} {
      display: none;
    }
  }

  .back_title {
    display: none;

    @media ${device.laptop} {
      display: block;
    }
  }
`;

export const BackLink = styled.a`
  position: absolute;
  width: 20px;
  height: 20px;
  left: -30px;
  display: flex;
  align-items: center;
  justify-content: center;

  @media ${device.laptop} {
    width: 13px;
    height: 20px;
    position: relative;
    margin-right: 4px;
    left: unset;
  }
`;

export const Title = styled.div`
  width: 100%;
  margin-bottom: 15px;
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;

  @media ${device.laptop} {
    font-size: 1.25em;
    justify-content: flex-start;
  }

  img {
    width: 40px;
    height: 40px;
    margin-right: 12px;

    @media ${device.laptop} {
      width: 50px;
      height: 50px;
      margin-right: 15px;
    }
  }

  div {
    width: 162px;
    font-size: 1em;
    font-weight: bold;
    text-align: left;

    @media ${device.laptop} {
      width: auto;
    }
  }
`;

export const FormWrapper = styled.div`
  width: 100%;
  height: 100%;
  padding: 95px 40px 40px 40px;
  display: flex;
  flex-direction: column;
  align-items: center;

  @media ${device.laptop} {
    width: auto;
    padding: 40px 40px 40px 174px;
  }

  form {
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;

    label {
      margin-bottom: 36px;

      @media ${device.laptop} {
        margin-bottom: 66px;
      }
    }

    @media ${device.laptop} {
      align-items: flex-end;
    }
  }
`;

export const Column = styled.div`
  position: relative;
  width: 100%;
  height: auto;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  & label + label {
    margin-left: 10px;
  }
`;

export const Button = styled(ButtonBasic)`
  margin-top: 10px;
  width: 100%;

  @media ${device.laptop} {
    width: auto;
  }
`;

export const BreadCrumbWide = styled.div`
  width: 100%;
  height: 22px;
  margin-bottom: 44px;
  display: none;
  font-size: 0.8125em;
  color: ${colors.primary};

  @media ${device.laptop} {
    display: flex;
    flex-direction: row;
    justify-content: flex-end;
    align-items: center;
  }
`;

export const Bullet = styled.span`
  width: 22px;
  height: 22px;
  margin: 0 15px;
  font-weight: 700;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid ${colors.primary};
  border-radius: 50%;

  &.check {
    background-color: ${colors.primary};
  }
`;

export const StepTitle = styled.span``;

export const Next = styled.span`
  display: flex;
  align-items: center;
  margin-left: 15px;
`;
