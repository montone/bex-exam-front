import React, { useState, useRef } from 'react';
import { Form } from '@unform/web';
import * as Yup from 'yup';

import { MdChevronLeft, MdChevronRight, MdCheck } from 'react-icons/md';

import useCardNumberProcess from '~/hooks/useCardNumberProcess';
import api from '~/services/api';
import history from '~/services/history';

import CreditCard from './CreditCard';
import IconCreditCard from '~/assets/icons/icon_credit-card.svg';
import { Input, Select } from '~/components/Form';

import {
  Section,
  CardContainer,
  BreadCrumb,
  BackLink,
  Title,
  FormWrapper,
  BreadCrumbWide,
  Bullet,
  StepTitle,
  Next,
  Column,
  Button,
} from './styles';

function Checkout() {
  const formRef = useRef(null);
  const [cardFlip, setCardFlip] = useState(false);
  const [cardNumber, setCardNumber] = useState('');
  const [cardName, setCardName] = useState('');
  const [cardVerification, setCardVerification] = useState('');
  const [cardExpires, setCardExpires] = useState('');
  const [cardInstallment, setCardInstallment] = useState('');
  const checkoutPhase = 2;

  const { cardNumberProcessed } = useCardNumberProcess(cardNumber);

  async function handleSubmit(data) {
    try {
      // Remove Previous Errors
      formRef.current.setErrors({});

      const schema = Yup.object().shape({
        cardnumber: Yup.string().required('Número de cartão inválido'),
        name: Yup.string()
          .min(10, 'Insira seu nome completo como está no cartão')
          .required('Campo Obrigatório'),
        expires: Yup.string()
          .required('Campo Obrigatório')
          .matches(/^\d{1,2}\/\d{1,2}$/, 'Data Inválida'),
        verification: Yup.string()
          .required('Campo Obrigatório')
          .min(3)
          .max(3)
          .matches(/^[0-9]{3,4}$/, 'Código Inválido'),
        installment: Yup.number().required('Insira o número de parcelas'),
      });

      await schema.validate(data, {
        abortEarly: false,
      });

      // Validation passed
      // Dummy api post

      const response = await api.post('/pagar', data);

      history.push('/pago', { payload: response.data });
    } catch (err) {
      const validationErrors = {};
      if (err instanceof Yup.ValidationError) {
        err.inner.forEach((error) => {
          validationErrors[error.path] = error.message;
        });
        formRef.current.setErrors(validationErrors);
      }
    }
  }

  function handleFormInput(e) {
    formRef.current.setErrors({});

    switch (e.target.name) {
      case 'cardnumber':
        setCardNumber(e.target.value);
        break;
      case 'name':
        setCardName(e.target.value);
        break;
      case 'expires':
        setCardExpires(e.target.value);
        break;
      case 'installment':
        setCardInstallment(e.target.value);
        break;
      case 'verification':
        setCardVerification(e.target.value);
        break;
      default:
        break;
    }
  }

  return (
    <Section>
      <CardContainer>
        <BreadCrumb>
          <BackLink>
            <MdChevronLeft size="20" />
          </BackLink>
          <span className="steps">
            <strong>Etapa 2</strong> de 3
          </span>
          <span className="back_title">Alterar Forma de Pagamento</span>
        </BreadCrumb>
        <Title>
          <img src={IconCreditCard} aria-hidden="true" alt="Icon Credit Card" />
          <div>
            <span>Adicione um novo Cartão de Crédito</span>
          </div>
        </Title>
        <CreditCard
          cardFlip={cardFlip}
          cardData={{
            cardNumberProcessed,
            cardName,
            cardVerification,
            cardExpires,
            cardInstallment,
          }}
        />
      </CardContainer>
      <FormWrapper>
        <BreadCrumbWide>
          <Bullet className={checkoutPhase > 1 ? 'check' : ''}>
            {checkoutPhase > 1 ? <MdCheck color="#fff" /> : '1'}
          </Bullet>
          <StepTitle>Carrinho</StepTitle>
          <Next>
            <MdChevronRight size="22" />
          </Next>

          <Bullet className={checkoutPhase > 2 ? 'check' : ''}>
            {checkoutPhase > 2 ? <MdCheck color="#fff" /> : '2'}
          </Bullet>
          <StepTitle>Pagamento</StepTitle>
          <Next>
            <MdChevronRight size="22" />
          </Next>
          <Bullet>3</Bullet>
          <StepTitle>Pagamento</StepTitle>
        </BreadCrumbWide>

        <Form
          ref={formRef}
          onSubmit={handleSubmit}
          onInput={(e) => handleFormInput(e)}
          autoComplete="off"
        >
          <Input
            name="cardnumber"
            label="Número do Cartão"
            inputMode="numeric"
            maxLength="16"
            tabIndex="0"
          />
          <Input name="name" label="Nome (igual ao cartão)" />
          <Column>
            <Input name="expires" label="Validade" inputmode="numeric" />
            <Input
              name="verification"
              label="CVV"
              info="Código de Verificação"
              inputmode="numeric"
              maxLength="3"
              onFocus={() => setCardFlip(true)}
              onBlur={() => setCardFlip(false)}
            />
          </Column>
          <Select
            name="installment"
            label="Numero de Parcelas"
            options={[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]}
          />
          <Button type="submit">Continuar</Button>
        </Form>
      </FormWrapper>
    </Section>
  );
}

export default Checkout;
