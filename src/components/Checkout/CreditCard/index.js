import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import CreditCardBgFront from '~/assets/creditcard/creditcard_bg_front.svg';
import CreditCardEmptyBgFront from '~/assets/creditcard/creditcard_empty_bg_front.svg';
import CreditCardBgBack from '~/assets/creditcard/creditcard_bg_back.svg';
import CreditCardEmptyBgBack from '~/assets/creditcard/creditcard_empty_bg_back.svg';
import CreditCardFlagVisa from '~/assets/creditcard/craeditcard_flag_visa.png';

import {
  Container,
  Card,
  CardFront,
  CardFlag,
  CardNumber,
  CardOwner,
  CardName,
  CardExpireDate,
  CardBackground,
  CardBack,
  CardSafetyCode,
} from './styles';

function CreditCard({ cardFlip, cardData }) {
  const {
    cardNumberProcessed,
    cardName,
    cardVerification,
    cardExpires,
  } = cardData;
  const [cardEmpty, setCardEmpty] = useState(true);

  useEffect(() => {
    if (/^[0-9]{2}/.test(cardNumberProcessed)) {
      setCardEmpty(false);
    } else {
      setCardEmpty(true);
    }
  }, [cardNumberProcessed]);

  return (
    <Container>
      <Card className={cardFlip && 'card_flip'}>
        <CardFront>
          {/^[0-9]{2}/.test(cardNumberProcessed) && (
            <CardFlag src={CreditCardFlagVisa} />
          )}
          <CardNumber>
            {cardNumberProcessed.map((item, i) => (
              <span key={i}>{item}</span>
            ))}
          </CardNumber>
          <CardOwner>
            <CardName>{cardName}</CardName>
            <CardExpireDate>{cardExpires}</CardExpireDate>
          </CardOwner>
          <CardBackground
            src={cardEmpty ? CreditCardEmptyBgFront : CreditCardBgFront}
          />
        </CardFront>
        <CardBack>
          <CardSafetyCode>{cardVerification.replace(/./g, '*')}</CardSafetyCode>
          <CardBackground
            src={cardEmpty ? CreditCardEmptyBgBack : CreditCardBgBack}
          />
        </CardBack>
      </Card>
    </Container>
  );
}

CreditCard.propTypes = {
  cardFlip: PropTypes.bool.isRequired,
  cardData: PropTypes.shape({
    cardNumberProcessed: PropTypes.arrayOf(PropTypes.string),
    cardName: PropTypes.string,
    cardVerification: PropTypes.string,
    cardExpires: PropTypes.string,
  }),
};

CreditCard.defaultProps = {
  cardData: {
    cardNumberProcessed: '',
    cardName: '',
    cardVerification: '',
    cardExpires: '',
  },
};

export default CreditCard;
