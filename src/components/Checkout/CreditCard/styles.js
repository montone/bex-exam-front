import styled from 'styled-components';

import colors from '~/styles/colors';
import { device } from '~/styles/queries';

export const Container = styled.div`
  width: 280px;
  height: 172px;
  background-color: blueviolet;
  position: absolute;
  top: 142px;
  left: 50%;
  font-family: -apple-system, system-ui, BlinkMacSystemFont, 'Segoe UI', Roboto,
    'Helvetica Neue', Arial, sans-serif;
  color: ${colors.white};
  text-shadow: 0px 1px 2px #000000b3;
  transform: translateX(-50%);
  background: no-repeat;
  border-radius: 8px;

  @media ${device.laptop} {
    width: 365px;
    height: 224px;
    top: 204px;
    left: 64px;
    transform: translateX(0);
  }
`;

export const Card = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  transform-style: preserve-3d;
  transition: all 0.5s ease;

  ::after {
    content: '';
    position: absolute;
    top: 29px;
    left: 50%;
    transform: translateX(-50%);
    width: 90%;
    height: 80%;
    box-shadow: 0 10px 12px rgba(0, 0, 0, 0.2);
    border-radius: 8px;
    z-index: -1;

    @media ${device.laptop} {
      box-shadow: 0 28px 12px rgba(0, 0, 0, 0.2);
    }
  }

  &.card_flip {
    transform: rotateY(180deg);
  }
`;

export const CardFront = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
`;

export const CardFlag = styled.img`
  position: absolute;
  top: 30px;
  left: 14px;
`;

export const CardNumber = styled.div`
  width: 100%;
  padding: 0 14px;
  position: absolute;
  top: 79px;
  font-size: 1.1875em;
  display: flex;
  justify-content: space-between;

  @media ${device.laptop} {
    font-size: 1.5em;
    padding: 0 34px;
  }
`;

export const CardOwner = styled.div`
  position: absolute;
  width: 100%;
  padding: 0 14px;
  bottom: 28px;
  font-size: 0.75em;
  display: flex;
  flex-direction: row;
  justify-content: space-between;

  @media ${device.laptop} {
    font-size: 1.25em;
    padding: 0 34px;
    bottom: 40px;
  }
`;

export const CardName = styled.div`
  text-transform: uppercase;
`;

export const CardExpireDate = styled.div``;

export const CardBack = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  backface-visibility: hidden;
  transform: rotateY(180deg);
`;

export const CardSafetyCode = styled.span`
  position: absolute;
  width: 45px;
  height: 19px;
  top: 82px;
  left: 129px;
  font-size: 1.1875em;
  color: ${colors.black};
  text-shadow: none;

  @media ${device.laptop} {
    left: 174px;
    top: 110px;
  }
`;

export const CardBackground = styled.img`
  width: 100%;
  height: auto;
  z-index: 10;
`;
