export default function useCardNumberProcess(number) {
  const cardNumberProcessed = number.padEnd(16, '*').match(/[\s\S]{1,4}/g);

  return { cardNumberProcessed };
}
