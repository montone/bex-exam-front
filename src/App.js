import React from 'react';
import { Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import '~/config/ReactotronConfig';

import Routes from '~/routes';
import history from '~/services/history';

import GlobalStyles from '~/styles/global';

import { store, persistor } from '~/store';

import MainLayout from '~/pages/_Layout/MainLayout';

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Router history={history}>
          <MainLayout>
            <Routes />
          </MainLayout>
          <GlobalStyles />
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
