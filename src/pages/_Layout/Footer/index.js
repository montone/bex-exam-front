import React from 'react';

import { Wrapper, Container, Placeholder } from './styles';

function Footer() {
  return (
    <Wrapper>
      <Container>
        <Placeholder />
      </Container>
    </Wrapper>
  );
}

export default Footer;
