import styled from 'styled-components';

import colors from '~/styles/colors';

import PageContainer from '~/components/PageContainer';

export const Wrapper = styled.header`
  width: 100%;
  height: 68px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${colors.white};
`;

export const Container = styled(PageContainer)`
  align-items: center;
  justify-content: center;
`;

export const Placeholder = styled.div`
  width: 80%;
  height: 12px;
  border-radius: 5px;
  background: ${colors.greyMedium};
`;
