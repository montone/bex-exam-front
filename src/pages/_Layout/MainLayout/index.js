import React from 'react';
import PropTypes from 'prop-types';

import PageWrapper from '~/components/PageWrapper';
import Header from '~/pages/_Layout/Header';
import Footer from '~/pages/_Layout/Footer';

function MainLayout({ children }) {
  return (
    <PageWrapper>
      <Header />
      {children}
      <Footer />
    </PageWrapper>
  );
}

MainLayout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element,
    PropTypes.func,
    PropTypes.node,
  ]).isRequired,
};

export default MainLayout;
