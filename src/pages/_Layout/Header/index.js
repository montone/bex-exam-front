import React from 'react';

import LogoDemoShop from '~/assets/logos/logo-demo-shop.svg';

import { Wrapper, Container } from './styles';

function Header() {
  return (
    <Wrapper>
      <Container>
        <img src={LogoDemoShop} alt="DemoShop, a sua melhor opção" />
      </Container>
    </Wrapper>
  );
}

export default Header;
