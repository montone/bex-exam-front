import styled from 'styled-components';

import colors from '~/styles/colors';

import { device } from '~/styles/queries';

import PageContainer from '~/components/PageContainer';

export const Wrapper = styled.header`
  width: 100%;
  height: 64px;
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${colors.white};

  @media ${device.laptop} {
    height: 92px;
  }
`;

export const Container = styled(PageContainer)`
  padding: 8px;
  align-items: center;

  @media ${device.laptop} {
    padding: 0;
  }

  img {
    width: 160px;
    height: auto;

    @media ${device.tabletLs} {
      width: 260px;
    }
  }
`;
