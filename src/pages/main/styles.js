import styled from 'styled-components';

import PageContainer from '~/components/PageContainer';

import { device } from '~/styles/queries';

export const Container = styled(PageContainer)`
  flex-direction: column;
  align-items: center;
  justify-content: center;

  @media ${device.laptop} {
    padding: 65px 0;
    flex-direction: row;
    align-items: flex-start;
  }

  @media ${device.desktop} {
    justify-content: space-between;
  }
`;
