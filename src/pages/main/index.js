import React from 'react';

import Checkout from '~/components/Checkout';
import Aside from '~/components/Aside';

import { Container } from './styles';

function Main() {
  return (
    <Container>
      <Checkout />
      <Aside />
    </Container>
  );
}

export default Main;
