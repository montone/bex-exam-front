import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`

  *, *::after, *::before {
   margin: 0;
   padding: 0;
   outline: none;
   box-sizing: border-box;
   -webkit-tap-highlight-color: transparent;
 }

 :focus {
    outline: none !important;
}

 html, body, #root {
   min-height: 100vh;
   min-width: 100vw;
 }

 body {
   -webkit-font-smoothing: antialiased;
   text-rendering: optimizeLegibility;
 }

 body, input, button, select, textarea, ul, ol {
   font-family: Verdana, Geneva, Tahoma, sans-serif;
   font-weight: 300;
 }

 form input {
   border: none;
 }

 button {
   border: none;
 }
 `;
