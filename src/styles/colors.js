export default {
  primary: '#DE4B4B',
  primaryLight: '#DE4B4B',
  black: '#3C3C3C',
  greyHeavy: '#707070',
  greyMedium: '#C9C9C9',
  grey: '#E6E6E6',
  greyLight: '#F7F7F7',
  white: '#ffffff',
  statusInfo: '#31A6EF',
  statusSuccess: '#75B812',
  statusWarning: '#F6BB1D',
  statusDanger: '#DA3331',
};
