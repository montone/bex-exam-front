import { createMuiTheme } from '@material-ui/core';

import colors from './colors';

const materialTheme = createMuiTheme({
  palette: {
    primary: {
      main: colors.primary,
    },
  },
});

export default materialTheme;
