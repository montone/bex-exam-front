import useCardNumberProcess from '~/hooks/useCardNumberProcess';

describe('Hooks > useCardNumberProcess', () => {
  it('should return 4 strings os asterisks', () => {
    const { cardNumberProcessed } = useCardNumberProcess('');

    expect(cardNumberProcessed).toStrictEqual(['****', '****', '****', '****']);
  });

  it('should modify the first array', () => {
    const { cardNumberProcessed } = useCardNumberProcess('1');

    expect(cardNumberProcessed[0]).toStrictEqual('1***');
  });

  it('array index string should correspond to string slice', () => {
    const { cardNumberProcessed } = useCardNumberProcess('6457382456725467');

    expect(cardNumberProcessed[1]).toStrictEqual('3824');
  });
});
